$(document).ready(function(){
    var socket = io();

    $('.previous').click(function(){
      window.$_GET = new URLSearchParams(location.search);
      var page = $_GET.get('page');
      if(page == null){
         return;
      }else if(page > 1){
        page1 = parseInt(page) - 1;
        $('.previous').attr('href', `/get?page=${page1}`);
        return;
      }else{
        $('.previous').attr('href', `/get?page=${page}`);
        return;
      }
    });

    $('.next').click(function(){
      window.$_GET = new URLSearchParams(location.search);
      var page = $_GET.get('page');
      if(page == null){
        $('.next').attr('href', `/get?page=2`);
        return;
      }else if(page < max){
        page1 = parseInt(page) + 1;
        $('.next').attr('href', `/get?page=${page1}`);
        return;
      }else{
        $('.next').attr('href', `/get?page=${page}`);
        return;
      }
    });

    $('.search').click(function() {
      var domain = $('.search-domain').val();
      domain = domain.replace(/\s/g, '');
      if(!domain){
        return;
      }else{
        socket.emit('search', {'domain': domain});
      }
    });

    socket.on('search', function(data){
       if(data.stt == 'error'){
        return;
      }else{
        if(data.domain == null){
          $('.tbody1').empty();
          $('.show1').empty();
          $('.tbody1').append(`<p class=\"notfond\">${$('.search-domain').val()} not found</p>`);
        }else{
          $('.tbody1').empty();
          $('.tbody1').empty();
          $('.tbody1').append(`<tr><td>${data.domain}</td><td>${data.status}</td><td><img src="/assets/images/${data.status}.png"></td></tr>`);
        }
      }
    });
});