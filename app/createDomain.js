var mongoose = require('mongoose');
var urlconnect = require('./config/config').mongodb;
var mongoosePaginate = require('mongoose-paginate');

var newDomain = function(){
	mongoose.Promise = global.Promise;
	var Schema = mongoose.Schema;
	var Domain = new Schema({
		_id: {
		    type: mongoose.Schema.Types.ObjectId,
		    index: true,
		    required: true,
		    auto: true,
		  },
		domain 	     : { type: String, required: true, unique: true},
		date_create  : { type: Date},
		status		 : { type: Number}
	});
	Domain.plugin(mongoosePaginate);
	var History = new Schema({

	});
	mongoose.connect(urlconnect, {useMongoClient: true});
	return mongoose.model('domains', Domain);	
}

module.exports = newDomain();