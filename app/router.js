var addDomain = require('./program').addDomain;
var showDomain = require('./program').showDomain;
var deleteDomain = require('./program').deleteDomain;
var searchDomain = require('./program').searchDomain;
module.exports = function(app){

	app.get('/', function(req, res, next){
		var page = req.query.page;
		var limit = req.query.limit;
		showDomain(page, limit, function(data){
			res.json({'page': data.page ,'domain': data.docs});
			res.end();
		});
	});

	app.get('/get', function(req, res, next){
		var page = req.query.page;
		var limit = req.query.limit;
		showDomain(page, limit, function(data){
			var pages = Array(data.pages).fill().map((e,i)=>i+1);
			res.render('load', {'page': data.page ,'domain': data.docs, 'total': data.total, 'pages': pages, 'max': data.pages});
		});
	});

	app.get('/Search/:domain', function(req, res){
		var domain = req.params.domain;
		searchDomain(domain, function(data){
			res.json(data);
			res.end();
		});
	});

	app.get('/Add/:domain', function(req, res){
		var domain = req.params.domain;
		addDomain(domain, function(data){
			res.json({'report':data});
			res.end();	
		});
	});

	app.get('/Delete/:domain', function(req, res){
		var domain = req.params.domain;
		deleteDomain(domain, function(data){
			res.json({'report':data});
			res.end();
		});
	});
}
