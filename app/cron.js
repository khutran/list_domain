var connect = require('./createDomain');
var checkstatusDomain = require('./program').checkstatusDomain;
var updateDomain = require('./program').editDomain;
const EventEmitter = require('events');
class MyEmitter extends EventEmitter {}
const myEmitter = new MyEmitter();

var update_statusDomain = function(){
	connect.paginate({}, {page: 1, limit:5}, function(error, domain){
		if(error){
			process.exit(1);
		}else{
			var pages = Array(domain.pages).fill().map((e,i)=>i+1);
			var page = pages[Math.floor(Math.random()*pages.length)];
			connect.paginate({},{page: page, limit: 5}, function(err, data){
				if(err){
					process.exit(1);
				}else{
					dem = 0;
					lengthpage = data.docs.length;
					data.docs.map(function(results){
						checkstatusDomain(results.domain)
						.then(function(status){
							if(status.stt == 'suscess'){
								updateDomain(status.domain, status.statuscode, function(data1){
									if(data1.status == 'error'){
										process.exit(1);
									}else{
										dem +=1;
										myEmitter.emit('finish',{'dem': dem, 'lengthpage': lengthpage});
									}
								});
							}else{
								dem +=1;
								myEmitter.emit('finish',{'dem': dem, 'lengthpage': lengthpage});
							}
						});
					});
				}
			});
		}
	});
}

myEmitter.on('finish', function(data){
	if(data.dem == data.lengthpage){
		process.exit(-1);
	}
});

update_statusDomain();
