var connect = require('./createDomain');

module.exports = function(io){
	io.on('connection', function(socket){
		socket.on('search', function(data){
			connect.findOne({'domain': data.domain}, function(error, domain){
				if(error){
					socket.emit('search', {'stt': 'error','results': error});
				}else if(domain == null){
					socket.emit('search', {'stt': 'suscess','domain': null});
				}else{
					socket.emit('search', {'stt': 'suscess','domain': domain.domain, 'status': domain.status});
				}
			});
		});
	});
}