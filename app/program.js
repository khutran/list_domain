var connect = require('./createDomain');
var https = require('https');
var http = require('http');
var Q = require('q');

var check_statusDomain = function(domain){
	return Q.Promise(function(response){
		http.get(`http://${domain}`, function(res){
			
			if((res.statusCode === 301 || res.statusCode === 302 || res.statusCode === 303) && res.headers.location.indexOf('https') === -1){
				http.get(`${res.headers.location}`, function(res2){
					return response({'stt': 'suscess','domain': `${domain}`, 'statuscode': res2.statusCode});
				}).on('error', function(e){
					return response({'stt': 'error','domain': `${domain}`, 'statuscode': e.message});
				});	
			}else if((res.statusCode === 301 || res.statusCode === 302 || res.statusCode === 303) && res.headers.location.indexOf('https') != -1){
				https.get(`${res.headers.location}`, function(res2){
					https.get(`${res2.headers.location}`, function(res2){
						return response({'stt': 'suscess','domain': `${domain}`, 'statuscode': res2.statusCode});
					})
				}).on('error', function(e){
					return response({'stt': 'error','domain': `${domain}`, 'statuscode': e.message});
				});	
			}else{
				return response({'stt': 'suscess', 'domain': `${domain}`, 'statuscode': res.statusCode});
			}
		}).on('error', function(e){
			return response({'stt': 'error','domain': `${domain}`, 'statuscode': e.message});
		});			
	});
}

var edit_Domain = function(domain, status, callback){
	connect.update({'domain': domain}, {$set:{'status': status}}, function(error, results){
		if(error){
			return callback({'status': 'error', 'data': error});
		}else{
			return callback({'status': 'suscess', 'data': results});
		}
	});
}

var add_Domain = function(domain, callback){
	connect.findOne({'domain': domain}, function(error, results){
		if(error){
			return callback(error);
		}else if(results){
			return callback({'results':'domain exits'});
		}else{
			check_statusDomain(domain)
			.then(function(results){
				if(results.stt == 'suscess'){
					var new_domain = ({'domain': results.domain, 'date_create': Date(), 'status': results.statuscode});
					var newdomain = new connect(new_domain);
					newdomain.save(function(err){
						if(err){
							return callback({'results': err});
						}else{
							return callback({'results': `${domain} add suscess`, 'statusCode': results.statuscode});
						}
					});
				}else{
					return callback({'results': `${domain} add error`, 'statusCode': results.statuscode});
				}	
			});
		}
	});
}	

var delete_Domain = function(domain, callback){
	check_statusDomain(domain)
	.then(function(results){
		connect.remove({'domain': results.domain}, function(error, data){
			if(error){
				return callback(error);
			}else{
				if(data.result.n === 1){
					return callback(`delete ${domain} suscess`);
				}else{
					return callback(`${domain} not found`);
				}
			}
		});
	});

}

var search_Domain = function(domain, callback){
	connect.findOne({'domain': domain}, function(error, domain){
		if(error){
			return callback(error);
		}else{
			return callback(domain);
		}
	});
}

var show_Domain = function(page, limit, callback){
	if(!page) var page = 1;
	if(!limit) var limit = 20;
	connect.paginate({}, {page: page, limit: limit}, function(error, domain){
		if(error){
			return callback(error);
		}else{
			return callback(domain);
		}
	});
}

module.exports = {
		addDomain:add_Domain,
		showDomain:show_Domain,
		deleteDomain:delete_Domain,
		checkstatusDomain:check_statusDomain,
		editDomain:edit_Domain,
		searchDomain:search_Domain
}

