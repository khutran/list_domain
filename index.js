var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

app.use(express.static(__dirname + '/'));
app.set('views', __dirname + '/assets/views');
app.set('view engine', 'ejs');

require('./app/router')(app);
require('./app/socketio')(io);

server.listen('3000');
